/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Alexander Bleuvito Fevrier - 6182001033
 */
import java.util.Scanner;

public class Divisor {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        double a, b, res;
        long startTime, endTime, duration;
        while (true) {
            System.out.println("DIVISOR APP");
            System.out.println("-----------");
            
            System.out.printf("%s", "Masukkan bilangan desimal pertama: ");
            a = sc.nextDouble();

            System.out.printf("%s", "Masukkan bilangan desimal kedua: ");
            b = sc.nextDouble();
            System.out.println("");

            if (b == 0) {
                System.out.println("Terjadi pembagian dengan 0");
                System.out.println("");
            } else {
                startTime = System.nanoTime();
                res = a / b;
                endTime = System.nanoTime();
                System.out.printf("%s %.2f", "Hasil: ", res);
                System.out.println("");

                duration = (endTime - startTime);
                System.out.println("Waktu yang dibutuhkan: " + duration + " ms");
                System.out.println("tes");
            }
        }
    }
}
